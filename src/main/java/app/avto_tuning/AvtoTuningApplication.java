package app.avto_tuning;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AvtoTuningApplication {

    public static void main(String[] args) {
        SpringApplication.run(AvtoTuningApplication.class, args);
    }

}
