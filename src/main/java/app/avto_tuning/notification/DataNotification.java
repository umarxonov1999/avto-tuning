package app.avto_tuning.notification;

import lombok.*;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DataNotification {
    private long id;
    private String title;
//    private String message;
    private String message;

}
