package app.avto_tuning.notification;

import app.avto_tuning.repository.FirebaseRepository;
import app.avto_tuning.repository.RoleRepository;
import app.avto_tuning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class FirebaseService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;
    @Autowired
    PasswordEncoder passwordEncoder;
    @Autowired
    FirebaseRepository firebaseRepository;

//    @Value("${firebase.fcm.server.key}")
//    private String firebaseKey;

//    public void notificationUser(String title, String message, User user) {
//        firebaseRepository.findByUser(user).forEach(firebase -> {
//            NotificationTest bodyNode = new NotificationTest();
//            bodyNode.setTo("/topics/"+firebase.getFirebase());
//            bodyNode.setData(new DataNotification(8455, title, message));
//            HttpHeaders headers = new HttpHeaders();
//            headers.setContentType(MediaType.APPLICATION_JSON);
//            headers.set("Authorization", "key=" + "AAAANjDJ9IA:APA91bGKP_eiCBv9IpiYTeJFWej5rvTdjCqjnkeXwzfhndXX5B38TYgSmav5wrTK9VNtnBLGBlv-wvLbnwVMr7dDAEuHkMDM5rOUueMpnoKy09eBWBgGfDxZ_FpMwSkbhckq_4QiDNmI" + "");
//            String url = "https://fcm.googleapis.com/fcm/send";
//            RestTemplate template = new RestTemplate();
//            HttpEntity<NotificationTest> entity = new HttpEntity<>(bodyNode, headers);
//            template.postForLocation(url, entity);
//            System.out.println("send mew message");
//        });
//
//    }
}
