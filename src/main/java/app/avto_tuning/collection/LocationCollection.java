package app.avto_tuning.collection;

import java.util.UUID;

public interface LocationCollection {
    UUID getId();
    String getName();
    Double getLan();
    Double getLat();
}
