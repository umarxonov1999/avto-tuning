package app.avto_tuning.controller;

import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.OperatorDto;
import app.avto_tuning.security.CurrentUser;
import app.avto_tuning.service.OperatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/operator")
public class OperatorController {
    @Autowired
    OperatorService operatorService;

    @PostMapping
    public ApiResponse saveOrEditOperator(@RequestBody OperatorDto operatorDto, @CurrentUser User user) {
        return operatorService.saveOrEditOperator(operatorDto, user);
    }

    @GetMapping("/{id}")
    public ApiResponse getOperator(@CurrentUser User user) {
        return operatorService.getOperator(user);
    }

    @GetMapping("/getAllOperator")
    public ApiResponse getAllOperator(@RequestParam(value = "page", defaultValue = "0") int page,
                                        @RequestParam(value = "size", defaultValue = "10") int size) {
        return operatorService.getAllOperator(page, size);
    }


    @GetMapping("/remove/{id}")
    public ApiResponse deleteOperator(@PathVariable UUID id) {
        return operatorService.enableOperator(id);
    }
}
