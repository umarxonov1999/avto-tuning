package app.avto_tuning.controller;

import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.BlogDto;
import app.avto_tuning.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/article")
public class BlogController {
    @Autowired
    BlogService blogService;

    @PostMapping
    public ApiResponse saveOrEditBlog(@RequestBody BlogDto blogDto) {
        return blogService.saveOrEditArticle(blogDto);
    }

    @GetMapping("/{id}")
    public ApiResponse getBlog(@PathVariable UUID id) {
        return blogService.getBlog(id);
    }

    @GetMapping("/by/{url}")
    public ApiResponse getBlogByUrl(@PathVariable String url) {
        return blogService.getBlogByUrl(url);
    }

//    @GetMapping("/getArticleList")
//    public HttpEntity<?> getArticleList(@RequestParam(value = "page", defaultValue = "0") int page,
//                                        @RequestParam(value = "size", defaultValue = "10") int size) {
//        Double Double = articleService.getArticleList(page, size, false);
//        return ResponseEntity.status(Double.isSuccess() ? 200 : 409).body(Double);
//    }
//
//    @GetMapping("/getArticleListView")
//    public HttpEntity<?> getArticleListView(@RequestParam(value = "page", defaultValue = "0") int page,
//                                            @RequestParam(value = "size", defaultValue = "10") int size) {
//        Double Double = articleService.getArticleList(page, size, true);
//        return ResponseEntity.ok(Double);
//    }

    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteBlog(@PathVariable UUID id) {
        return blogService.deleteBlog(id);
    }
}
