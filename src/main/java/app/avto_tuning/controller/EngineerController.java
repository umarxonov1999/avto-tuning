package app.avto_tuning.controller;

import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.EngineerDto;
import app.avto_tuning.security.CurrentUser;
import app.avto_tuning.service.EngineerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/engineer")
public class EngineerController {
    @Autowired
    EngineerService engineerService;

    @PostMapping
    public ApiResponse saveOrEditEngineer(@RequestBody EngineerDto engineerDto, @CurrentUser User user) {
        return engineerService.saveOrEditEngineer(engineerDto, user);
    }

    @GetMapping("/{id}")
    public ApiResponse getEngineer(@CurrentUser User user) {
        return engineerService.getEngineer(user);
    }

    @GetMapping("/getAllEngineer")
    public ApiResponse getAllEngineer(@RequestParam(value = "page", defaultValue = "0") int page,
                                      @RequestParam(value = "size", defaultValue = "10") int size) {
        return engineerService.getAllEngineer(page, size);
    }

    @GetMapping("/remove/{id}")
    public ApiResponse deleteEngineer(@PathVariable UUID id) {
        return engineerService.enableEngineer(id);
    }
}
