package app.avto_tuning.controller;

import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ProductDto;
import app.avto_tuning.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/product")
public class ProductController {
    @Autowired
    ProductService productService;

    @PostMapping
    public ApiResponse saveOrEditProduct(@RequestBody ProductDto productDto) {
        return productService.saveOrEditProduct(productDto);
    }

    @GetMapping("/{id}")
    public ApiResponse getProduct(@PathVariable UUID id) {
        return productService.getProduct(id);
    }

    @GetMapping("/getAllProduct")
    public ApiResponse getProductPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                           @RequestParam(value = "size", defaultValue = "10") int size) {
        return productService.getAllProduct(page, size);
    }

    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteProduct(@PathVariable UUID id) {
        return productService.deleteProduct(id);
    }
}
