package app.avto_tuning.controller;

import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.CompanyDto;
import app.avto_tuning.security.CurrentUser;
import app.avto_tuning.service.CompanyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartRequest;

import java.util.UUID;

@RestController
@RequestMapping("/api/company")
public class CompanyController {
    @Autowired
    CompanyService companyService;

    @PostMapping
    public ApiResponse saveOrEditCompany(@RequestBody CompanyDto companyDto, @CurrentUser User user) {
        return companyService.saveOrEditCompany(companyDto, user);
    }

    @GetMapping("/{id}")
    public ApiResponse getAdminCompany(@PathVariable UUID id) {
        return companyService.getClientCompany(id);
    }

//    @GetMapping("/{id}")
//    public ApiResponse getCompany(@RequestParam Company company) {
//        return companyService.getCompany(company);
//    }

    @GetMapping("/getAllCompany")
    public ApiResponse getAllCompany(@RequestParam(value = "page", defaultValue = "0") int page,
                                     @RequestParam(value = "size", defaultValue = "10") int size) {
        return companyService.getAllCompany(page, size);
    }

    @GetMapping("/remove/{id}")
    public ApiResponse enableCompany(@PathVariable UUID id) {
        return companyService.enableCompany(id);
    }

    @GetMapping("/selectedService")
    public ApiResponse selectedService(@RequestParam int R,
                                       @RequestParam Double lan,
                                       @RequestParam Double lat) {
        return companyService.selectedService(R, lan, lat);
    }

//    @PostMapping("/savePhoto/{id}")
//    public ApiResponse savePhoto(MultipartRequest multipartRequest, @PathVariable UUID id){
//        return companyService.setPhotoCompany(multipartRequest,id);
//    }
}
