package app.avto_tuning.controller;

import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ErrorsField;
import app.avto_tuning.payload.UserDto;
import app.avto_tuning.repository.UserRepository;
import app.avto_tuning.security.CurrentUser;
import app.avto_tuning.security.JwtTokenProvider;
import app.avto_tuning.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping("/api/user")
public class UserController {

    @Autowired
    UserService userService;

    @Autowired
    UserRepository userRepository;

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    JwtTokenProvider jwtTokenProvider;

    @PostMapping("/login")
    public ApiResponse login(@Valid @RequestBody UserDto userDto, BindingResult error) {
        try {
            if (error.hasErrors()) {
                return new ApiResponse("feild",
                        HttpStatus.CONFLICT.value(),
                        error.getFieldErrors().stream().map(fieldError -> new ErrorsField(fieldError.getField(),
                                fieldError.getDefaultMessage())));
            }
            System.out.println(userDto);
            if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber())) {

                Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                        userDto.getPhoneNumber(),
                        userDto.getPassword()
                ));
                String token = jwtTokenProvider.generateToken(authentication);
                return new ApiResponse("login",
                        HttpStatus.OK.value(),
                        token);
            } else {
                return new ApiResponse("Bunday foydalanuvchi mavjud emas ",
                        HttpStatus.CONFLICT.value(),
                        false);
            }
        } catch (Exception e) {

            System.out.println(e);

            return new ApiResponse("Error password",
                    HttpStatus.CONFLICT.value(),
                    e.getMessage());
        }
    }

    @GetMapping("/me")
    public HttpEntity<?> userMe(@CurrentUser User user) {
        if (user != null) {
            UserDto userDto = userService.getUserDto(user);
            return ResponseEntity.status(HttpStatus.OK.value()).body(userDto);
        } else {
            return ResponseEntity.status(HttpStatus.CONFLICT.value()).body("Error");
        }
    }

    @PostMapping("/firebase/{firebase}")
    public ApiResponse firebase(@CurrentUser User user, @PathVariable String firebase) {
        return userService.saveFirebase(firebase,
                user);
    }

    @PostMapping("/register")
    public ApiResponse saveUser(@RequestBody UserDto userDto) {
        return userService.saveUser(userDto);
    }

    @PutMapping("/editPassword")
    public ApiResponse editPassword(@RequestBody UserDto userDto) {
        return userService.editPassword(userDto);
    }

    @PutMapping("/editUser")
    public ApiResponse editUser(@RequestBody UserDto userDto) {
        return userService.editUser(userDto);
    }

    @GetMapping("/remove/{id}")
    public ApiResponse enabledUser(@PathVariable UUID id) {
        return userService.enabledUser(id);
    }

    @ExceptionHandler
    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    public ApiResponse handleException(@CurrentUser User user, Exception e) {
        return new ApiResponse(e.getMessage(),
                HttpStatus.BAD_REQUEST.value(),
                e.hashCode());
    }

}
