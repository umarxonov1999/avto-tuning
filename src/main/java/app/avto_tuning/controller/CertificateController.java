package app.avto_tuning.controller;

import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.CertificateDto;
import app.avto_tuning.service.CertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/certificate")
public class CertificateController {
    @Autowired
    CertificateService certificateService;

    @PostMapping
    public ApiResponse saveOrEditCertificate(@RequestBody CertificateDto carouselDto) {
        return certificateService.saveOrEditCertificate(carouselDto);
    }

    @GetMapping("/{id}")
    public ApiResponse getCertificate(@PathVariable UUID id) {
        return certificateService.getCertificate(id);
    }

    @GetMapping("/getAllCertificate")
    public ApiResponse getCertificatePage(@RequestParam(value = "page", defaultValue = "0") int page,
                                          @RequestParam(value = "size", defaultValue = "10") int size) {
        return certificateService.getAllCertificate(page, size);
    }

    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteCertificate(@PathVariable UUID id) {
        return certificateService.deleteCertificate(id);
    }
}
