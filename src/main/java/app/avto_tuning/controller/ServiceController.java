package app.avto_tuning.controller;

import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ServiceDto;
import app.avto_tuning.service.ServicesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@RestController
@RequestMapping("/api/services")
public class ServiceController {
    @Autowired
    ServicesService serviceService;

    @PostMapping
    public ApiResponse saveOrEditServices(@RequestBody ServiceDto serviceDto) {
        return serviceService.saveOrEditService(serviceDto);
    }

    @GetMapping("/{id}")
    public ApiResponse getServices(@PathVariable UUID id) {
        return serviceService.getService(id);
    }

    @GetMapping("/getAllServices")
    public ApiResponse getServicesPage(@RequestParam(value = "page", defaultValue = "0") int page,
                                         @RequestParam(value = "size", defaultValue = "10") int size) {
       return serviceService.getAllService(page, size);
    }

    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteServices(@PathVariable UUID id) {
        return serviceService.deleteService(id);
    }
}
