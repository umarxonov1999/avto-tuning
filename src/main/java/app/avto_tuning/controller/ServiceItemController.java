package app.avto_tuning.controller;

import app.avto_tuning.entity.ServiceItem;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ServiceItemDto;
import app.avto_tuning.service.ServiceItemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/serviceItem")
public class ServiceItemController {
    @Autowired
    ServiceItemService serviceItemService;

    @PostMapping
    public ApiResponse saveOrEditServiceItem(@RequestBody ServiceItemDto serviceItemDto) {
        return serviceItemService.saveOrEditServiceItem(serviceItemDto);
    }

        @GetMapping("/{id}")
    public ApiResponse getServiceItem(@RequestBody ServiceItem serviceItem) {
        return new ApiResponse("message", HttpStatus.OK.value(), serviceItemService.getServiceItem(serviceItem));
    }
    @DeleteMapping("/remove/{id}")
    public ApiResponse deleteServiceItem(@PathVariable UUID id) {
        return serviceItemService.deleteServiceItem(id);
    }
}
