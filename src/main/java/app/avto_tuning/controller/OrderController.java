package app.avto_tuning.controller;

import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.OrderDto;
import app.avto_tuning.security.CurrentUser;
import app.avto_tuning.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@Controller
@RequestMapping("/api/order")
public class OrderController {
    @Autowired
    OrderService orderService;

    @PostMapping
    public ApiResponse saveOrEditOrder(@RequestBody OrderDto orderDto, @CurrentUser User user) {
        return orderService.saveOrEditOrder(orderDto, user);
    }

    @GetMapping("/{id}")
    public ApiResponse getOrder(@CurrentUser User user) {
        return orderService.getOrder(user);
    }

    @GetMapping("/getAllOrder")
    public ApiResponse getAllOrder(@RequestParam(value = "page", defaultValue = "0") int page,
                                   @RequestParam(value = "size", defaultValue = "10") int size) {
        return orderService.getAllOrder(page, size);
    }

    @GetMapping("/enableOrder")
    public ApiResponse enableOrder(@PathVariable UUID id) {
        return orderService.enableOrder(id);
    }
}
