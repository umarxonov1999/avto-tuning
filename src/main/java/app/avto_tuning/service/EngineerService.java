package app.avto_tuning.service;

import app.avto_tuning.entity.Engineer;
import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.EngineerDto;
import app.avto_tuning.repository.CompanyRepository;
import app.avto_tuning.repository.EngineerRepository;
import app.avto_tuning.repository.WorkStatusRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class EngineerService {
    @Autowired
    EngineerRepository engineerRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    WorkStatusRepository workStatusRepository;

    public ApiResponse saveOrEditEngineer(EngineerDto engineerDto, User user) {
        try {
            Engineer engineer = new Engineer();
            if (engineerDto.getId() != null) {
                engineer = engineerRepository.findById(engineerDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetEngineer"));
            }
            engineer.setUser(user);
            if (engineerDto.getCompanyId() == null)
                engineer.setCompany(companyRepository.findById(engineerDto.getCompanyId()).orElseThrow(() -> new ResourceNotFoundException("GetCompany")));
            if (engineerDto.getWorkStatusId() == null)
                engineer.setWorkStatus(workStatusRepository.findById(engineerDto.getWorkStatusId()).orElseThrow(() -> new ResourceNotFoundException("GetWorkStatus")));
            engineer.setCompany(engineer.getCompany());
            engineer.setWorkStatus(engineer.getWorkStatus());
            engineer.setWorkStartTime(engineerDto.getWorkStartTime() == null ? engineer.getWorkStartTime() : engineerDto.getWorkStartTime());
            engineer.setWorkEndTime(engineerDto.getWorkEndTime() == null ? engineer.getWorkEndTime() : engineerDto.getWorkEndTime());
            engineer.setSalaryPrice(engineerDto.getSalaryPrice());
            engineerRepository.save(engineer);
            return new ApiResponse("Enginer added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getEngineer(User user) {
        try {
            Engineer engineer = engineerRepository.findById(user.getId()).orElseThrow(() -> new ResourceNotFoundException("GetEngineer"));
            return new ApiResponse("Success", HttpStatus.OK.value(), engineer);
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllEngineer(int page, int size) {
        try {
            Sort engineerSort = Sort.by(Sort.Direction.DESC, "Egineer");
            Page<Engineer> allEngineer = engineerRepository.findAll(PageRequest.of(page, size, engineerSort));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allEngineer.getContent().stream().map(this::getEngineerDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public EngineerDto getEngineerDto(Engineer engineer) {
        return new EngineerDto(
                engineer.getId(),
                engineer.getUser().getId(),
                engineer.getCompany().getId(),
                engineer.getWorkStatus().getId(),
                engineer.getWorkStartTime(),
                engineer.getWorkEndTime(),
                engineer.getSalaryPrice()
        );
    }

    public ApiResponse enableEngineer(UUID id) {
        try {
            Optional<Engineer> engineerOptional = engineerRepository.findById(id);
            if (engineerOptional.isPresent()) {
                Engineer engineer = engineerOptional.get();
                boolean enabled = !engineer.isActive();
                engineer.setActive(enabled);

                engineerRepository.save(engineer);
                return new ApiResponse((enabled ? "active" : "isActive"), HttpStatus.OK.value());
            }
            return new ApiResponse("Success delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
