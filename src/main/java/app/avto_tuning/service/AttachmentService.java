package app.avto_tuning.service;

import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.AttachmentContent;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ReqFile;
import app.avto_tuning.repository.AttachmentContentRepository;
import app.avto_tuning.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

@Service
public class AttachmentService {

    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired

    AttachmentContentRepository attachmentContentRepository;


    /**
     * BU RASMNI YUKLASH UCHUN
     *
     * @param request
     * @return
     */
    public List<ReqFile> uploadFile(MultipartHttpServletRequest request) {
        try {
            List<ReqFile> uuidList = new ArrayList<>();
            Iterator<String> fileNames = request.getFileNames();
            while (fileNames.hasNext()) {
                MultipartFile file = request.getFile(fileNames.next());
                assert file != null;
                Attachment attachment = new Attachment();
                attachment.setName(file.getName());
                attachment.setSize(file.getSize());
                attachment.setContentType(file.getContentType());
                Attachment savedAttachment = attachmentRepository.save(attachment);

                AttachmentContent attachmentContent = new AttachmentContent(
                        savedAttachment,
                        file.getBytes());
                attachmentContentRepository.save(attachmentContent);
                uuidList.add(new ReqFile(savedAttachment.getId(),savedAttachment.getName(),savedAttachment.getSize(),
                        ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attachment/").path(String.valueOf(savedAttachment.getId())).toUriString()));
            }
            return uuidList;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * BU PRICING YARATISH UCHUN
     *
     * @param id
     * @return
     */
    public HttpEntity<?> getFile(UUID id) {
        Attachment attachment = attachmentRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getAttachment"));
        AttachmentContent attachmentContent = attachmentContentRepository.findByAttachmentId(id);
        return ResponseEntity.ok().contentType(MediaType.valueOf(attachment.getContentType()))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; fileName=\"" + attachment.getName() + "\"")
                .body(attachmentContent.getContent());
    }

    public ApiResponse deleteFile(UUID id) {
        try {
            attachmentContentRepository.deleteByAttachmentId(id);
            attachmentRepository.deleteById(id);
            return new ApiResponse("Deleted", HttpStatus.OK.value());
        }catch (Exception e){
            return new ApiResponse("Server error",HttpStatus.CONFLICT.value());
        }
    }
}
