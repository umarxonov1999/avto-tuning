package app.avto_tuning.service;

import app.avto_tuning.entity.Operator;
import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.OperatorDto;
import app.avto_tuning.repository.OperatorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OperatorService {
    @Autowired
    OperatorRepository operatorRepository;

    public ApiResponse saveOrEditOperator(OperatorDto operatorDto, User user) {
        try {
            Operator operator = new Operator();
            if (operatorDto.getId() != null) {
                operator = operatorRepository.findById(operatorDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetOperator"));
            }
            operator.setUser(user);
            operator.setStartTime(operatorDto.getStartTime() == null ? operator.getStartTime() : operatorDto.getStartTime());
            operator.setEndTime(operatorDto.getEndTime() == null ? operator.getEndTime() : operatorDto.getEndTime());
            operator.setAnswerOrderCount(operator.getAnswerOrderCount() + 1);
            operator.setActiveOperator(operatorDto.isActive());
            operatorRepository.save(operator);
            return new ApiResponse("Operator added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getOperator(User user) {
        try {
            Operator operator = operatorRepository.findById(user.getId()).orElseThrow(() -> new ResourceNotFoundException("GetOperator"));
            return new ApiResponse("Success", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllOperator(int page, int size) {
        try {
            Sort operatorSort = Sort.by(Sort.Direction.DESC, "operator");
            Page<Operator> allOperator = operatorRepository.findAll(PageRequest.of(page, size, operatorSort));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allOperator.getContent().stream().map(this::getOperatorDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public OperatorDto getOperatorDto(Operator operator) {
        return new OperatorDto(
                operator.getId(),
                operator.getUser().getId(),
                operator.getStartTime(),
                true,
                operator.getEndTime(),
                operator.getAnswerOrderCount(),
                operator.getLikes(),
                operator.getDisLike()
        );
    }

    public ApiResponse enableOperator(UUID id) {
        try {
            Optional<Operator> operatorOptional = operatorRepository.findById(id);
            if (operatorOptional.isPresent()) {
                Operator operator = operatorOptional.get();
                boolean enabled = !operator.isActiveOperator();
                operator.setActiveOperator(enabled);

                operatorRepository.save(operator);
                return new ApiResponse((enabled ? "active" : "isActive"), HttpStatus.OK.value());
            }
            return new ApiResponse("Success delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
