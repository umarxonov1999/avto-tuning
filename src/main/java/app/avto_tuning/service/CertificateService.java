package app.avto_tuning.service;

import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.Certificate;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.CertificateDto;
import app.avto_tuning.repository.AttachmentRepository;
import app.avto_tuning.repository.CertificateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class CertificateService {
    @Autowired
    CertificateRepository certificateRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveOrEditCertificate(CertificateDto certificateDto) {
        try {
            Certificate certificate = new Certificate();
            if (certificateDto.getId() != null) {
                certificate = certificateRepository.findById(certificateDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetCertificate"));
            }
            certificate.setTextUz(certificateDto.getTextUz() == null ? certificate.getTextUz() : certificateDto.getTextUz());
            certificate.setTextRu(certificateDto.getTextRu() == null ? certificate.getTextRu() : certificateDto.getTextRu());
            if (!certificateDto.getPhotoListId().isEmpty())
                certificate.setPhotoList(certificateDto.getPhotoListId() == null ? certificate.getPhotoList() : attachmentRepository.findAllById(certificateDto.getPhotoListId()));
            certificateRepository.save(certificate);
            return new ApiResponse("Certificate added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getCertificate(UUID id) {
        try {
            Certificate certificate = certificateRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("GetCertificate"));
            return new ApiResponse("Success", HttpStatus.OK.value(), certificate);
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllCertificate(int page, int size) {
        try {
            Page<Certificate> allCertificate = certificateRepository.findAll(PageRequest.of(page, size));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allCertificate.getContent().stream().map(this::getCertificateDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public CertificateDto getCertificateDto(Certificate certificate) {
        return new CertificateDto(
                certificate.getId(),
                certificate.getTextUz(),
                certificate.getTextRu(),
                getAttachmentUUID(certificate.getPhotoList())
        );
    }

    public List<UUID> getAttachmentUUID(List<Attachment> attachmentList) {
        List<UUID> uuidList = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            uuidList.add(attachment.getId());
        }
        return uuidList;
    }

    public ApiResponse deleteCertificate(UUID id) {
        try {
            certificateRepository.deleteById(id);
            return new ApiResponse("Success full delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }


}
