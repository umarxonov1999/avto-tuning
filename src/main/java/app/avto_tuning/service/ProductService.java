package app.avto_tuning.service;

import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.Product;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ProductDto;
import app.avto_tuning.repository.CompanyRepository;
import app.avto_tuning.repository.ProductBrandRepository;
import app.avto_tuning.repository.ProductModelRepository;
import app.avto_tuning.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ProductService {
    @Autowired
    ProductRepository productRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    ProductBrandRepository productBrandRepository;
    @Autowired
    ProductModelRepository productModelRepository;

    public ApiResponse saveOrEditProduct(ProductDto productDto) {
        try {
            Product product = new Product();
            if (productDto.getId() != null) {
                product = productRepository.findById(productDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetProduct"));
            }
            product.setName(productDto.getName() == null ? product.getName() : productDto.getName());
            if (productDto.getCompanyId() != null)
                product.setCompany(companyRepository.findById(productDto.getCompanyId()).orElseThrow(() -> new ResourceNotFoundException("GetCompany")));
            if (productDto.getProductBrandId() != null)
                product.setProductBrand(productBrandRepository.findById(productDto.getProductBrandId()).orElseThrow(() -> new ResourceNotFoundException("GetProductBrand")));
            if (productDto.getProductModelId() != null)
                product.setProductModel(productModelRepository.findById(productDto.getProductModelId()).orElseThrow(() -> new ResourceNotFoundException("GetProductModel")));
            product.setDescription(productDto.getDescription() == null ? product.getDescription() : productDto.getDescription());
            product.setCount(productDto.getCount() == null ? product.getCount() : productDto.getCount());
            product.setInComPrice(productDto.getInComPrice());
            product.setSalePrice(productDto.getSalePrice());
            productRepository.save(product);
            return new ApiResponse("Product added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getProduct(UUID id) {
        try {
            Product product = productRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("GetProduct"));
            return new ApiResponse("Success", HttpStatus.OK.value(), product);
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllProduct(int page, int size) {
        try {
            Page<Product> allProduct = productRepository.findAll(PageRequest.of(page, size));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allProduct.getContent().stream().map(this::getProductDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ProductDto getProductDto(Product product) {
        return new ProductDto(
                product.getId(),
                product.getName(),
                product.getCompany().getId(),
                product.getProductBrand().getId(),
                product.getProductModel().getId(),
                getAttachmentUUID(product.getPhotoList()),
                product.getDescription(),
                product.getInComPrice(),
                product.getInComPrice(),
                product.getCount()
        );
    }

    //**UUID larni list olib kelish**//
    public List<UUID> getAttachmentUUID(List<Attachment> attachmentList) {
        List<UUID> uuidList = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            uuidList.add(attachment.getId());
        }
        return uuidList;
    }

    public ApiResponse deleteProduct(UUID id) {
        try {
            productRepository.deleteById(id);
            return new ApiResponse("Success full delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
