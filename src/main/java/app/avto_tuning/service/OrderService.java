package app.avto_tuning.service;

import app.avto_tuning.entity.*;
import app.avto_tuning.notification.FirebaseService;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.OrderDto;
import app.avto_tuning.repository.*;
import org.aspectj.weaver.ast.Or;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class OrderService {
    @Autowired
    OrderRepository orderRepository;
    @Autowired
    CompanyRepository companyRepository;
    @Autowired
    ServiceItemRepository serviceItemRepository;
    @Autowired
    OperatorRepository operatorRepository;

//    @Autowired
//    FirebaseService firebaseService;


    public ApiResponse saveOrEditOrder(OrderDto orderDto, User user) {
        try {
            Order order = new Order();
            if (orderDto.getId() != null) {
                order = orderRepository.findById(order.getId()).orElseThrow(() -> new ResourceNotFoundException("GetOrder"));
            }
            order.setUser(user);
            if (orderDto.getCompanyId() != null)
                order.setCompany(orderDto.getCompanyId() == null ? order.getCompany() : companyRepository.findById(orderDto.getCompanyId()).orElseThrow(() -> new ResourceNotFoundException("GetCompany")));
            if (orderDto.getServiceItemId() != null)
                order.setServiceItem(orderDto.getServiceItemId() == null ? order.getServiceItem() : serviceItemRepository.findById(orderDto.getServiceItemId()).orElseThrow(() -> new ResourceNotFoundException("GetServiceItem")));
            if (orderDto.getOperatorId() != null)
                order.setOperator(orderDto.getOperatorId() == null ? order.getOperator() : operatorRepository.findById(orderDto.getOperatorId()).orElseThrow(() -> new ResourceNotFoundException("GetOperator")));
                order.setPriceType(order.getPriceType());
            order.setOrderNumber(order.getOrderNumber() + 1);
            order.setLat(orderDto.getLat() == null ? order.getLat() : orderDto.getLat());
            order.setLan(orderDto.getLan() == null ? order.getLan() : orderDto.getLan());
            orderRepository.save(order);

//            firebaseService.notificationUser("buyurtma", "+998917797278", companyRepository.findByUserId(user));
            return new ApiResponse("Order added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getOrder(User user) {
        try {
            Order order = orderRepository.findById(user.getId()).orElseThrow(() -> new ResourceNotFoundException("GetOrder"));
            return new ApiResponse("Success", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllOrder(int page, int size) {
        try {
            Sort ordersSort = Sort.by(Sort.Direction.DESC, "orderNumber");
            Page<Order> allOrder = orderRepository.findAll(PageRequest.of(page, size, ordersSort));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allOrder.getContent().stream().map(this::getOrderDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public OrderDto getOrderDto(Order order) {
        return new OrderDto(
                order.getId(),
                order.getOrderNumber(),
                order.getLat(),
                order.getLan()
        );
    }

    public ApiResponse enableOrder(UUID id) {
        try {
            Optional<Order> orderOptional=orderRepository.findById(id);
            if (orderOptional.isPresent()) {
                Order order = orderOptional.get();
                boolean enabled = !order.isOrderActivity();
                order.setOrderActivity(enabled);

                orderRepository.save(order);
                return new ApiResponse((enabled ? "active" : "isActive"), HttpStatus.OK.value());
            }
            return new ApiResponse("User not found", HttpStatus.CONFLICT.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
