package app.avto_tuning.service;

import app.avto_tuning.collection.LocationCollection;
import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.Company;
import app.avto_tuning.entity.User;
import app.avto_tuning.payload.*;
import app.avto_tuning.repository.AttachmentRepository;
import app.avto_tuning.repository.CompanyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartRequest;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class CompanyService {

    @Autowired
    CompanyRepository companyRepository;

    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveOrEditCompany(CompanyDto companyDto, User user) {
        try {
            Company company = new Company();
            if (companyDto.getId() != null) {
                company = companyRepository.findById(companyDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetCompany"));
            }
            company.setName(companyDto.getName() == null ? company.getName() : companyDto.getName());
            company.setLat(companyDto.getLat() == null ? company.getLat() : companyDto.getLat());
            company.setLan(companyDto.getLan() == null ? company.getLan() : companyDto.getLan());
            company.setPhoneNumber(companyDto.getPhoneNumber() == null ? company.getPhoneNumber() : companyDto.getPhoneNumber());
            company.setDescription(companyDto.getDescription() == null ? company.getDescription() : companyDto.getDescription());
            company.setUser(user);
            if (!companyDto.getPhotoList().isEmpty())
                company.setPhotoList(companyDto.getPhotoList() == null ? company.getPhotoList() : attachmentRepository.findAllById(companyDto.getPhotoList()));
            if (!companyDto.getLicencePhotoList().isEmpty()) {
                company.setLicencePhotoList(companyDto.getLicencePhotoList() == null ? company.getLicencePhotoList() : attachmentRepository.findAllById(companyDto.getLicencePhotoList()));
//
        }
            companyRepository.save(company);
            return new ApiResponse("Company added",
                    HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse activeService() {
        return new ApiResponse("List of active Locations",
                HttpStatus.OK.value(),
                companyRepository.findByActiveTrue());
    }

    public ApiResponse selectedService(int R, Double lan, Double lat) {
        try {
            if (R <= 0) {
                return new ApiResponse("All locations",
                        HttpStatus.OK.value(),
                        companyRepository.findByActiveTrue());
            } else if (lan == null
                    || lat == null) {
                return new ApiResponse("Enter the correct location",
                        HttpStatus.CONFLICT.value(),
                        null);
            } else {
                return new ApiResponse("NearBy locations",
                        HttpStatus.OK.value(),
                        nearByLocations(R,
                                lan,
                                lat));
            }
        } catch (Exception e) {
            return new ApiResponse(e.getMessage(),
                    HttpStatus.CONFLICT.value(),
                    null);
        }
    }

    private List<LocationCollection> nearByLocations(int R, Double lan, Double lat) {
        return companyRepository.findByActiveAndLanBetweenAndLatBetween(true,
                lan - 41.274525 * R,
                lan + 69.22538805376082 * R,
                lat - 41.27303702053479 * R,
                lat + 69.22538805376082 * R);
    }

    public ApiResponse getClientCompany(UUID id) {
        try {
            Company company = companyRepository.findById(id).get();
            return new ApiResponse("Success",
                    HttpStatus.OK.value(),
                    new ReqCompanyForClient(company.getId(),
                            company.getName(),
                            company.getLat(),
                            company.getLan(),
                            company.getPhoneNumber(),
                            company.getDescription(),
                            generationLinkPhoto(company.getPhotoList()),
                            generationLinkPhoto(company.getLicencePhotoList()),
                            company.getOrderCount()));
        } catch (Exception e) {
            return new ApiResponse("kalangga sani ishlasang ulasanmi :) ",
                    HttpStatus.CONFLICT.value());
        }
    }

    public List<ReqFile> generationLinkPhoto(List<Attachment> attachments) {
        List<ReqFile> listPhoto = new ArrayList<>();
        for (Attachment savedAttachment : attachments) {
            listPhoto.add(new ReqFile(savedAttachment.getId(),
                    savedAttachment.getName(),
                    savedAttachment.getSize(),
                    ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attachment/").path(String.valueOf(savedAttachment.getId())).toUriString()));
        }
        return listPhoto;
    }

    public RestCompany getCompany(Company company) {
        return new RestCompany(
                company.getId(),
                company.getName(),
                company.getLat(),
                company.getLan(),
                company.getPhoneNumber(),
                company.getDescription(),
                generationLinkPhoto(company.getPhotoList()),
                generationLinkPhoto(company.getLicencePhotoList())
        );
    }

//    public List<String> generationLinkPhoto(List<Attachment> attachments){
//        List<String> listPhoto=new ArrayList<>();
//        for (Attachment attachment : attachments) {
//            listPhoto.add(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attachment/").path(String.valueOf(attachment.getId())).toUriString());
//        }
//        return listPhoto;
//    }

    public ApiResponse getAllCompany(int page, int size) {
        try {
            Sort companySort = Sort.by(Sort.Direction.DESC,
                    "company");
            Page<Company> allCompany = companyRepository.findAll(PageRequest.of(page,
                    size,
                    companySort));
            return new ApiResponse("Success full",
                    HttpStatus.OK.value(),
                    allCompany.getContent().stream().map(this::getCompanyDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

    public CompanyDto getCompanyDto(Company company) {
        return new CompanyDto(
                company.getId(),
                company.getName(),
                company.getLat(),
                company.getLan(),
                company.getPhoneNumber(),
                company.getDescription(),
                null,
                null,
                null
        );
    }

    public ApiResponse enableCompany(UUID id) {
        try {
            Optional<Company> companyOptional = companyRepository.findById(id);
            if (companyOptional.isPresent()) {
                Company company = companyOptional.get();
                boolean enabled = !company.isActive();
                company.setActive(enabled);

                companyRepository.save(company);
                return new ApiResponse((enabled ? "active" : "isActive"),
                        HttpStatus.OK.value());
            }
            return new ApiResponse("User not found",
                    HttpStatus.CONFLICT.value());
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

//    public ApiResponse setPhotoCompany(MultipartRequest multipartRequest, UUID id) {
//    try {
//        if (companyRepository.existsById(id)){
//            multipartRequest.
//        }
//    }
}
