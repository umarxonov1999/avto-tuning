package app.avto_tuning.service;

import app.avto_tuning.entity.Blog;
import app.avto_tuning.entity.Attachment;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.BlogDto;
import app.avto_tuning.repository.BlogRepository;
import app.avto_tuning.repository.AttachmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class BlogService {
    @Autowired
    BlogRepository blogRepository;
    @Autowired
    AttachmentRepository attachmentRepository;

    public ApiResponse saveOrEditArticle(BlogDto blogDto) {
        try {
            Blog blog = new Blog();
            if (blogDto.getId() != null) {
                blog = blogRepository.findById(blogDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetArticle"));
            }
            blog.setTitle(blogDto.getTitle() == null ? blog.getTitle() : blogDto.getTitle());
            blog.setText(blogDto.getText() == null ? blog.getText() : blogDto.getText());
            blog.setUrl(blogDto.getUrl() == null ? blog.getUrl() : blogDto.getUrl());
            if (!blogDto.getPhotoListId().isEmpty())
                blog.setPhotoList(blogDto.getPhotoListId() == null ? blog.getPhotoList() : attachmentRepository.findAllById(blogDto.getPhotoListId()));
            blogRepository.save(blog);
            return new ApiResponse(blogDto.getId() != null ? "Blog edited" : "Blog added", HttpStatus.NO_CONTENT.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONTINUE.value());
        }
    }


    public ApiResponse getBlog(UUID id) {
        try {
            Blog blog = blogRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("getBlog"));
            return new ApiResponse("Success", HttpStatus.OK.value(), (getBlogDto(blog)));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getBlogByUrl(String url) {
        try {
            Optional<Blog> optionalArticle = blogRepository.findByUrl(url);
            return optionalArticle.map(blog -> new ApiResponse("Success", HttpStatus.OK.value(), (getBlogDto(blog)))).orElseGet(() -> new ApiResponse("Not found Blog", HttpStatus.OK.value()));

        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }


//    public ApiResponse getBlogList(int page, int size, boolean view) {
//        try {
//            Page<Article> allArticleList = articleRepository.findAll(PageRequest.of(page, size));
//            return new ApiResponse(
//                    allArticleList.getContent().stream().map(view ? this::getBlogDtoView : this::getBlogDto).collect(Collectors.toList()),
//
//                    allArticleList.getTotalPages()
//            );
//        } catch (Exception e) {
//            return new ApiResponse("Error", false);
//        }
//    }

    public BlogDto getBlogDto(Blog blog) {
        return new BlogDto(
                blog.getId(),
                blog.getTitle(),
                blog.getText(),
                getAttachmentUUID(blog.getPhotoList()), // Attachment id sini  listini olib kelish
                blog.getUrl()
//                getDateFromTimestamp(blog.getCreatedAt())
        );
    }

    // UUIDlarni listni yig'ib berish uchun ishlatiladigan mathod
    public List<UUID> getAttachmentUUID(List<Attachment> attachmentList) {
        List<UUID> uuidList = new ArrayList<>();
        for (Attachment attachment : attachmentList) {
            uuidList.add(attachment.getId());
        }
        return uuidList;
    }

    public BlogDto getBlogDtoView(Blog blog) {
        return new BlogDto(
                blog.getId(),
                blog.getTitle(),
                blog.getText(),
                null,
                blog.getUrl()
//                getDateFromTimestamp(blog.getCreatedAt())
        );
    }

    public String getDateFromTimestamp(Timestamp timestamp) {
        SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        return formatter.format(timestamp);
    }


    public ApiResponse deleteBlog(UUID id) {
        try {
            blogRepository.deleteById(id);
            return new ApiResponse("Success full delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}

