package app.avto_tuning.service;

import app.avto_tuning.entity.Services;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.ServiceDto;
import app.avto_tuning.repository.CompanyRepository;
import app.avto_tuning.repository.ServiceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class ServicesService {
    @Autowired
    ServiceRepository serviceRepository;
    @Autowired
    CompanyRepository companyRepository;

    public ApiResponse saveOrEditService(ServiceDto serviceDto) {
        try {
            Services services = new Services();
            if (serviceDto.getId() != null) {
                services = serviceRepository.findById(serviceDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetService"));
            }
            services.setName(serviceDto.getName() == null ? services.getName() : serviceDto.getName());
            if (serviceDto.getCompanyId() != null)
                services.setCompany(companyRepository.findById(services.getId()).orElseThrow(() -> new ResourceNotFoundException("GetCompany")));
            serviceRepository.save(services);
            return new ApiResponse("Service added", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getService(UUID id) {
        try {
            Services services = serviceRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("GetService"));
            return new ApiResponse("Success", HttpStatus.OK.value(), services);
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse getAllService(int page, int size) {
        try {
            Page<Services> allService = serviceRepository.findAll(PageRequest.of(page, size));
            return new ApiResponse("Success full", HttpStatus.OK.value(), allService.getContent().stream().map(this::getServiceDto).collect(Collectors.toList()));
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public ServiceDto getServiceDto(Services services) {
        return new ServiceDto(
                services.getId(),
                services.getName(),
                services.getDescription(),
                services.getCompany().getId()
        );
    }

    public ApiResponse deleteService(UUID id) {
        try {
            serviceRepository.deleteById(id);
            return new ApiResponse("Success full delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
