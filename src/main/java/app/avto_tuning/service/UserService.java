package app.avto_tuning.service;

import app.avto_tuning.entity.Firebase;
import app.avto_tuning.entity.User;
import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.UserDto;
import app.avto_tuning.repository.FirebaseRepository;
import app.avto_tuning.repository.RoleRepository;
import app.avto_tuning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService implements UserDetailsService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    FirebaseRepository firebaseRepository;


    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        return userRepository.findByPhoneNumber(s).orElseThrow(() -> new UsernameNotFoundException(s));
    }


    public ApiResponse saveUser(UserDto userDto) {
        try {
            if (!userRepository.existsByPhoneNumber(userDto.getPhoneNumber())) {

                User user = new User();

                user.setFirstName(userDto.getFirstName());
                user.setLastName(userDto.getLastName());
                user.setPhoneNumber(userDto.getPhoneNumber());
                user.setPassword(passwordEncoder.encode(userDto.getPassword()));

                /*TASDIQLASH*/
//                sendMessage("65146");
                userRepository.save(user);
                return new ApiResponse("User added",
                        HttpStatus.OK.value());
            } else {
                return new ApiResponse("Phone number already exist",
                        HttpStatus.CONFLICT.value());
            }
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse editUser(UserDto userDto) {
        try {
            Optional<User> optionalUser = userRepository.findById(userDto.getId());

            if (optionalUser.isPresent()) {
                User user = optionalUser.get();
                user.setFirstName(userDto.getFirstName() == null ? user.getFirstName() : userDto.getFirstName());
                user.setLastName(userDto.getLastName() == null ? user.getLastName() : userDto.getLastName());
                if (userRepository.existsByPhoneNumber(userDto.getPhoneNumber()))
                    return new ApiResponse("this number is in the database",
                            HttpStatus.OK.value());
                user.setPhoneNumber(userDto.getPhoneNumber() == null ? user.getPhoneNumber() : userDto.getPhoneNumber());
                userRepository.save(user);
                return new ApiResponse("User edit",
                        HttpStatus.OK.value());
            } else {
                return new ApiResponse("Not found",
                        HttpStatus.CONFLICT.value());
            }
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse editPassword(UserDto userDto) {
        if (userDto.getNewPassword().equals(userDto.getPrePassword())) {
            Optional<User> userOptional = userRepository.findById(userDto.getId());
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                if (passwordEncoder.matches(userDto.getOldPassword(),
                        user.getPassword())) {
                    user.setPassword(passwordEncoder.encode(userDto.getNewPassword()));
                    userRepository.save(user);
                    return new ApiResponse("Password edit",
                            HttpStatus.OK.value());
                }
                return new ApiResponse("Error",
                        HttpStatus.CONFLICT.value());
            }
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
        return new ApiResponse("Error",
                HttpStatus.CONFLICT.value());
    }

//    public ApiResponse getAllUser(int page, int size) {
//        try {
//            Page<User> allUser = userRepository.findAll(PageRequest.of(page, size));
//            return new ApiResponse("Success", HttpStatus.OK.value(), allUser.getContent().stream().map(this::getUserDto).collect(Collectors.toList()));
//        } catch (Exception e) {
//            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
//        }
//    }

    public UserDto getUserDto(User user) {
        return new UserDto(
                user.getId(),
                user.getFirstName(),
                user.getLastName(),
                user.getPhoneNumber(),
                null,
                user.getDescriptionUz(),
                user.getDescriptionRu(),
                user.getLat(),
                user.getLan(),
                null,
                null,
                null,
                user.isEnabled()
        );
    }

    public ApiResponse enabledUser(UUID id) {
        try {
            Optional<User> userOptional = userRepository.findById(id);
            if (userOptional.isPresent()) {
                User user = userOptional.get();
                boolean enabled = !user.isEnabled();
                user.setEnabled(enabled);

                userRepository.save(user);
                return new ApiResponse((enabled ? "active" : "isActive"),
                        HttpStatus.OK.value());
            }
            return new ApiResponse("User not found",
                    HttpStatus.CONFLICT.value());
        } catch (Exception e) {
            return new ApiResponse("Error",
                    HttpStatus.CONFLICT.value());
        }
    }

    public ApiResponse saveFirebase(String firebase, User user) {
        try {
            firebaseRepository.save(new Firebase(firebase,
                    user));
            return new ApiResponse("saved",
                    HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("not saved",
                    HttpStatus.CONFLICT.value());
        }
    }


//    private void sendMessage(String code) {
//        RestTemplate restTemplate = new RestTemplate();
//        String fooResourceUrl
//                = "https://api.telegram.org/bot1684071295:AAHqnNUVK2c5uwxtJqDwRuo3APbDyX5mIwI/sendMessage?chat_id=1602102173&text=";
//        ResponseEntity<ApiAnswer> response
//                = restTemplate.getForEntity(fooResourceUrl + code, ApiAnswer.class);
//        System.out.println(response);
//    }
}
