package app.avto_tuning.service;

import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.ServiceItem;

import app.avto_tuning.payload.ApiResponse;
import app.avto_tuning.payload.RestServiceItem;
import app.avto_tuning.payload.ServiceItemDto;
import app.avto_tuning.repository.AttachmentRepository;
import app.avto_tuning.repository.CarCategoryRepository;
import app.avto_tuning.repository.ServiceItemRepository;
import app.avto_tuning.repository.ServiceRepository;


import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class ServiceItemService {
    @Autowired
    ServiceItemRepository serviceItemRepository;
    @Autowired
    AttachmentRepository attachmentRepository;
    @Autowired
    CarCategoryRepository carCategoryRepository;
    @Autowired
    ServiceRepository serviceRepository;

    public ApiResponse saveOrEditServiceItem(ServiceItemDto serviceItemDto) {
        try {
            ServiceItem serviceItem = new ServiceItem();
            if (serviceItemDto.getId() != null) {
                serviceItem = serviceItemRepository.findById(serviceItemDto.getId()).orElseThrow(() -> new ResourceNotFoundException("GetServiceItem"));
            }
            serviceItem.setName(serviceItemDto.getNameUz() == null ? serviceItem.getName() : serviceItemDto.getNameUz());
            serviceItem.setDescription(serviceItemDto.getDescriptionRu() == null ? serviceItem.getDescription() : serviceItemDto.getDescriptionRu());
            if (!serviceItemDto.getPhotoList().isEmpty())
                serviceItem.setPhotoList(serviceItemDto.getPhotoList() == null ? serviceItem.getPhotoList() : attachmentRepository.findAllById(serviceItemDto.getPhotoList()));
            if (serviceItemDto.getCarCategoryId() != null)
                serviceItem.setCarCategory(serviceItemDto.getCarCategoryId() == null ? serviceItem.getCarCategory() : carCategoryRepository.findById(serviceItemDto.getCarCategoryId()).orElseThrow(() -> new ResourceNotFoundException("GetCarCategory")));
            if (serviceItemDto.getServicesId() != null)
                serviceItem.setServices(serviceItemDto.getServicesId() == null ? serviceItem.getServices() : serviceRepository.findById(serviceItemDto.getServicesId()).orElseThrow(() -> new ResourceNotFoundException("GetService")));
            serviceItemRepository.save(serviceItem);
            return new ApiResponse("ServiceItem", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }

    public RestServiceItem getServiceItem(ServiceItem serviceItem) {
        return new RestServiceItem(
                serviceItem.getId(),
                serviceItem.getName(),
                serviceItem.getDescription(),
                generationLinkPhoto(serviceItem.getPhotoList()),
                serviceItem.getServices().getId(),
                serviceItem.getCarCategory().getId()
        );
    }

    public List<String> generationLinkPhoto(List<Attachment> attachments) {
        List<String> listPhoto = new ArrayList<>();
        for (Attachment attachment : attachments) {
            listPhoto.add(ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/attachment/").path(String.valueOf(attachment.getId())).toUriString());
        }
        return listPhoto;
    }

    public ApiResponse deleteServiceItem(UUID id) {
        try {
            serviceItemRepository.deleteById(id);
            return new ApiResponse("Success delete", HttpStatus.OK.value());
        } catch (Exception e) {
            return new ApiResponse("Error", HttpStatus.CONFLICT.value());
        }
    }
}
