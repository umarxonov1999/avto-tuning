package app.avto_tuning.config;

import app.avto_tuning.entity.Company;
import app.avto_tuning.entity.Role;
import app.avto_tuning.entity.User;
import app.avto_tuning.entity.enums.RoleName;
import app.avto_tuning.repository.CompanyRepository;
import app.avto_tuning.repository.RoleRepository;
import app.avto_tuning.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataLoader implements CommandLineRunner {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    CompanyRepository companyRepository;

    @Value("${spring.datasource.initialization-mode}")
    private String addSuperAdmin;


    @Override
    public void run(String... args) throws Exception {
        if (addSuperAdmin.equals("always")) {
            Role byRoleName = roleRepository.findByRoleName(RoleName.ROLE_SUPER_ADMIN);
            Set<Role> role = new HashSet<>(Arrays.asList(byRoleName));
            User user = new User(
                    "Asomiddin",
                    "Raxmiddinov",
                    "+998999596877",
                    passwordEncoder.encode("root1234"),
                    null,
                    41.281056763846415,
                    69.27980697990391,
                    null,
                    null,
                    0,
                    true,
                    role,
                    true,
                    true,
                    true,
                    true);
            userRepository.save(user);

//            for (double i = 0; i < 10; i=i+0.009) {
//                companyRepository.save(new Company(
//                        "BMW service",
//                        41.27353496221772+i,
//                        69.22578938864957+i,
//                        "+9989999999"+i,
//                        true
//                ));
//            }
        }
    }
}
