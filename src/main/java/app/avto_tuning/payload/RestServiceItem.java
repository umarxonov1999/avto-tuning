package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestServiceItem {
    private UUID id;
    private String name;
    private String description;
    private List<String> photoList;
    private UUID servicesId;
    private UUID carCategoryId;
}
