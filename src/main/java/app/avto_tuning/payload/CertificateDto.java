package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CertificateDto {
    private UUID id;
    private String textUz;
    private String textRu;
    private List<UUID> photoListId;
}
