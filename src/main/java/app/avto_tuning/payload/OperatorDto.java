package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OperatorDto {
    private UUID id;
    private UUID userId;
    private Date startTime;
    private boolean active;
    private Date endTime;
    private long answerOrderCount;
    private long like;
    private long disLike;

}
