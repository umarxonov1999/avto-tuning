package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ApiResponse {
    private String message;
    private int success;
    private Object object;

    public ApiResponse(String message, int success) {
        this.message = message;
        this.success = success;
    }

}
