package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RestCompany {
    private UUID id;
    private String name;
    private Double lat;
    private Double lan;
    private String phoneNumber;
    private String description;
    private List<ReqFile> photoList;
    private List<ReqFile> licencePhotoList;


}
