package app.avto_tuning.payload;

import app.avto_tuning.entity.Company;
import app.avto_tuning.entity.User;
import app.avto_tuning.entity.WorkStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class EngineerDto {
    private UUID id;
    private UUID userId;
    private UUID companyId;
    private UUID workStatusId;
    private Date workStartTime;
    private Date workEndTime;
    private double salaryPrice;
}
