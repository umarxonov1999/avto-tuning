package app.avto_tuning.payload;

import app.avto_tuning.entity.Attachment;
import app.avto_tuning.entity.Company;
import app.avto_tuning.entity.ProductBrand;
import app.avto_tuning.entity.ProductModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {
    private UUID id;
    private String name;
    private UUID companyId;
    private UUID productBrandId;
    private UUID productModelId;
    private List<UUID> photoListId;
    private String description;
    private double inComPrice;
    private double salePrice;
    private Integer count;
}
