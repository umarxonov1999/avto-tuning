package app.avto_tuning.payload;

import app.avto_tuning.entity.CarCategory;
import app.avto_tuning.entity.ServiceType;
import app.avto_tuning.entity.Services;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServiceItemDto {
    private UUID id;
    private String nameUz;
    private String nameRu;
    private String descriptionUz;
    private String descriptionRu;
    private List<UUID> photoList;
    private UUID servicesId;
    private UUID carCategoryId;
}
