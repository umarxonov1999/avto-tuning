package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CarCategoryDto {
    private UUID id;
    private String name;
    private UUID carBrandId;
}
