package app.avto_tuning.payload;

import app.avto_tuning.entity.enums.PriceType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {
    private UUID id;
    private Long orderNumber;
    private UUID companyId;
    private UUID serviceItemId;
    private PriceType priceType;
    private UUID operatorId;
    private Double lat;
    private Double lan;
    private Date date;
    private String description;

    public OrderDto(UUID id, Long orderNumber, Double lat, Double lan) {
        this.id = id;
        this.orderNumber = orderNumber;
        this.lat = lat;
        this.lan = lan;
    }
}
