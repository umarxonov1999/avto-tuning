package app.avto_tuning.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BlogDto {
    private UUID id;
    private String title;
    private String text;
    private List<UUID> photoListId;
    private String url;
//    private String date;
}
