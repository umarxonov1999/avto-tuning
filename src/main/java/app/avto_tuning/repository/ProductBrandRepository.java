package app.avto_tuning.repository;

import app.avto_tuning.entity.ProductBrand;
import app.avto_tuning.projection.CustomProductBrand;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "productBrand", collectionResourceRel = "list", excerptProjection = CustomProductBrand.class)
public interface ProductBrandRepository extends JpaRepository<ProductBrand, UUID> {

}
