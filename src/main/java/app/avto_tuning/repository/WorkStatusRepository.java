package app.avto_tuning.repository;

import app.avto_tuning.entity.WorkStatus;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface WorkStatusRepository extends JpaRepository<WorkStatus, UUID> {
}
