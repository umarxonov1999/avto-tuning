package app.avto_tuning.repository;

import app.avto_tuning.collection.LocationCollection;
import app.avto_tuning.entity.Company;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface CompanyRepository extends JpaRepository<Company, UUID> {

    List<LocationCollection> findByActiveTrue();

    List<LocationCollection> findByActiveAndLanBetweenAndLatBetween(boolean active, Double lan, Double lan2, Double lat, Double lat2);

    Optional<Company> findById(UUID id);

//    @Query(value = "SELECT * from Company where user_id=:userId", nativeQuery = true)
//    Company findByIdQuery(UUID id);

    Company findByUserId(UUID user_id);
}