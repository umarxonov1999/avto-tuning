package app.avto_tuning.repository;

import app.avto_tuning.entity.ServiceItem;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface ServiceItemRepository extends JpaRepository<ServiceItem, UUID> {
}
