package app.avto_tuning.repository;

import app.avto_tuning.entity.Role;
import app.avto_tuning.entity.enums.RoleName;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface RoleRepository extends JpaRepository<Role, UUID> {
    Role findByRoleName(RoleName roleName);
}
