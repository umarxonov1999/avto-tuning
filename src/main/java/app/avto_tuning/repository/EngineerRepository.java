package app.avto_tuning.repository;

import app.avto_tuning.entity.Engineer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface EngineerRepository extends JpaRepository<Engineer, UUID> {
}
