package app.avto_tuning.repository;

import app.avto_tuning.entity.ProductModel;
import app.avto_tuning.projection.CustomProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "productModel", collectionResourceRel = "list", excerptProjection = CustomProductModel.class)
public interface ProductModelRepository extends JpaRepository<ProductModel, UUID> {

}
