package app.avto_tuning.repository;

import app.avto_tuning.entity.AttachmentContent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

public interface AttachmentContentRepository extends JpaRepository<AttachmentContent, UUID> {
    void deleteByAttachmentId(UUID attachment_id);

    AttachmentContent findByAttachmentId(UUID id);
}
