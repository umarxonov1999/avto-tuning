package app.avto_tuning.repository;

import app.avto_tuning.entity.CarBrand;
import app.avto_tuning.projection.CustomCarBrand;
import app.avto_tuning.projection.CustomProductModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "carBrand", collectionResourceRel = "list", excerptProjection = CustomCarBrand.class)
public interface CarBrandRepository extends JpaRepository<CarBrand, UUID> {

}
