package app.avto_tuning.repository;

import app.avto_tuning.entity.Firebase;
import app.avto_tuning.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

public interface FirebaseRepository extends JpaRepository<Firebase, UUID> {
    List<Firebase> findByUser(User user);
}
