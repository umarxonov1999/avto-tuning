package app.avto_tuning.repository;

import app.avto_tuning.entity.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<User, UUID> {
    @Query(nativeQuery = true, value = "SELECT * FROM users WHERE deleted=false AND id IN (SELECT user_id FROM user_role WHERE role_id = (SELECT id FROM role WHERE role_name = :roleName))")
    Page<User> findAllByRole(Pageable pageable, String roleName);

    Optional<User> findByPhoneNumber(String phoneNumber);

    boolean existsByPhoneNumber(String phoneNumber);

//    @Query(value = "select * from USER u where u.phoneNumber=:phoneNumber and u.password=:password",nativeQuery = true)
//    boolean findByPhoneNumberAndPassword(String phoneNumber, String password);

    @Query(value = "SELECT * FROM User u WHERE u.phoneNumber = :phoneNumber and u.password = :password",nativeQuery = true)
    User findByPhoneNumberAndPassword(@Param("phoneNumber") String phoneNumber,@Param("password") String password);
}
