package app.avto_tuning.repository;

import app.avto_tuning.entity.CarBrand;
import app.avto_tuning.entity.CarCategory;
import app.avto_tuning.projection.CustomCarCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.UUID;

@RepositoryRestResource(path = "carCategory", collectionResourceRel = "list", excerptProjection = CustomCarCategory.class)
public interface CarCategoryRepository extends JpaRepository<CarCategory, UUID> {
}
