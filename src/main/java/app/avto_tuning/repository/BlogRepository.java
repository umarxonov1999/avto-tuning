package app.avto_tuning.repository;

import app.avto_tuning.entity.Blog;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

public interface BlogRepository extends JpaRepository<Blog, UUID> {
    boolean existsByUrl(String newUrl);

    Optional<Blog> findByUrl(String url);
}
