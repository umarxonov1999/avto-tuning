package app.avto_tuning.projection;

import app.avto_tuning.entity.ProductBrand;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customProductBrand", types = ProductBrand.class)
public interface CustomProductBrand {
    UUID getId();

    String getName();

    UUID getCarCategoryId();
}
