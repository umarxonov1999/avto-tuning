package app.avto_tuning.projection;

import app.avto_tuning.entity.CarBrand;
import app.avto_tuning.entity.ProductModel;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCarBrand", types = CarBrand.class)
public interface CustomCarBrand {
    UUID getId();

    String getName();

    String getDescription();
}
