package app.avto_tuning.projection;

import app.avto_tuning.entity.CarBrand;
import app.avto_tuning.entity.CarCategory;
import org.springframework.data.rest.core.config.Projection;

import java.util.UUID;

@Projection(name = "customCarCategory", types = CarCategory.class)
public interface CustomCarCategory {
    UUID getId();

    String getName();

    String getDescription();

    String getCarBrand();
}
