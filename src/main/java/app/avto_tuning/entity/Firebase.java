package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Firebase extends AbsEntity {
    @Column(unique = true)
    private String firebase;

    @ManyToOne
    private User user;
}
