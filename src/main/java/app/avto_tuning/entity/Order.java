package app.avto_tuning.entity;

import app.avto_tuning.entity.enums.PriceType;
import app.avto_tuning.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity(name = "orders")
public class Order extends AbsEntity {

    @Column(nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long orderNumber;

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceItem serviceItem;

    @Enumerated(EnumType.STRING)
    private PriceType priceType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Operator operator;

    @Column(nullable = false)
    private Double lat;

    @Column(nullable = false)
    private Double lan;

    @Column(nullable = false)
    private Timestamp orderTime;

    private boolean orderActivity = true;
}
