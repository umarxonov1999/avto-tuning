package app.avto_tuning.entity.enums;

public enum RoleName {
    ROLE_SUPER_ADMIN, ROLE_ADMIN, ROLE_ENGINEER, ROLE_USER,ROLE_OPERATOR

}
