package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import app.avto_tuning.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Product extends AbsNameEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private Company company;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductBrand productBrand;

    @ManyToOne(fetch = FetchType.LAZY)
    private ProductModel productModel;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Attachment> photoList;

    @Column(nullable = false)
    private double inComPrice;

    @Column(nullable = false)
    private double salePrice;

    @Column(nullable = false)
    private Integer count;
}
