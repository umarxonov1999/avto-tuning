package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Blog extends AbsEntity {
    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "text")
    private String text;

    @Column(unique = true, columnDefinition = "text")
    private String url;

    @OneToMany
    private List<Attachment> photoList;
}
