package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import app.avto_tuning.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class CarCategory extends AbsNameEntity {

    @ManyToOne(fetch = FetchType.LAZY)
    private CarBrand carBrand;

}
