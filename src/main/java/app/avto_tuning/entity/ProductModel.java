package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ProductModel extends AbsEntity {
    @Column(nullable = false)
    private String key;

    @Column(nullable = false)
    private String value;
}
