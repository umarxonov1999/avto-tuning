package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Certificate extends AbsEntity {
    @Column(nullable = false)
    private String textUz;

    @Column(nullable = false)
    private String textRu;

    @OneToMany(fetch = FetchType.LAZY)
    private List<Attachment> photoList;
}
