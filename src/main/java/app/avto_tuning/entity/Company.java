package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Company extends AbsNameEntity {
    public Company(Double lat, Double lan, String phoneNumber, boolean active) {

//        this.user = user;
        this.lat = lat;
        this.lan = lan;
        this.phoneNumber = phoneNumber;
        this.active = active;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    private User user;

    private Double lat;

    private Double lan;


    @ManyToMany(fetch = FetchType.LAZY)
    private List<Attachment> licencePhotoList;

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Attachment> photoList;

    @Column(nullable = false)
    private String phoneNumber;;

    private String description;

    private boolean delete=false;

    private boolean active=false ;

    private Double money;

    private long orderCount;
}
