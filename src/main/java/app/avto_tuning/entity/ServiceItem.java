package app.avto_tuning.entity;

import app.avto_tuning.entity.template.AbsNameEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class ServiceItem extends AbsNameEntity {
    //xizmat turlari bo'ladi

    @ManyToMany(fetch = FetchType.LAZY)
    private List<Attachment> photoList;

    @ManyToOne(fetch = FetchType.LAZY)
    private ServiceType serviceType;

    @ManyToOne(fetch = FetchType.LAZY)
    private Services services;

    @ManyToOne(fetch = FetchType.LAZY)
    private CarCategory carCategory;

    private Timestamp totalTime;

    private boolean active=true;

    private double price;
}
